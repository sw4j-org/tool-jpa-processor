/*
 * Copyright (C) 2017 Uwe Plonus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.sw4j.tool.annotation.jpa.liquibase.v34.entity;

import org.sw4j.tool.annotation.jpa.util.ITSuperclass;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author Uwe Plonus
 */
public class FieldObjectEntityIT extends ITSuperclass {

    @BeforeClass()
    public static void setUpEntities() throws Exception {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(1L);
        getEm().persist(entity);
    }

    @Test
    public void testCreateEntity() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        getEm().persist(entity);
    }

    @Test
    public void testFindEntity() {
        FieldObjectEntity entity = getEm().find(FieldObjectEntity.class, 1L);
        Assert.assertNotNull(entity, "Expected an entity to be found.");
        Assert.assertEquals(entity.getId(), 1L, "Expected the entity to be found.");
    }

    @Test
    public void testMappingLongObject() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setLongObject(102L);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getLongObject(), (Long)102L, "Expected the mapped value.");
    }

    @Test
    public void testMappingLongObjectMaxValue() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setLongObject(Long.MAX_VALUE);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getLongObject(), (Long)Long.MAX_VALUE, "Expected the mapped value.");
    }

    @Test
    public void testMappingLongObjectMinValue() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setLongObject(Long.MIN_VALUE);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getLongObject(), (Long)Long.MIN_VALUE, "Expected the mapped value.");
    }

    @Test
    public void testMappingIntObject() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setIntObject(102);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getIntObject(), (Integer)102, "Expected the mapped value.");
    }

    @Test
    public void testMappingIntObjectMaxValue() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setIntObject(Integer.MAX_VALUE);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getIntObject(), (Integer)Integer.MAX_VALUE, "Expected the mapped value.");
    }

    @Test
    public void testMappingIntObjectMinValue() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setIntObject(Integer.MIN_VALUE);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getIntObject(), (Integer)Integer.MIN_VALUE, "Expected the mapped value.");
    }

    @Test
    public void testMappingShortObject() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setShortObject((short)102);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getShortObject(), Short.valueOf((short)102), "Expected the mapped value.");
    }

    @Test
    public void testMappingShortObjectMaxValue() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setShortObject(Short.MAX_VALUE);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getShortObject(), Short.valueOf(Short.MAX_VALUE), "Expected the mapped value.");
    }

    @Test
    public void testMappingShortObjectMinValue() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setShortObject(Short.MIN_VALUE);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getShortObject(), Short.valueOf(Short.MIN_VALUE), "Expected the mapped value.");
    }

    @Test
    public void testMappingByteObject() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setByteObject((byte)102);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getByteObject(), Byte.valueOf((byte)102), "Expected the mapped value.");
    }

    @Test
    public void testMappingByteObjectMaxValue() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setByteObject(Byte.MAX_VALUE);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getByteObject(), Byte.valueOf(Byte.MAX_VALUE), "Expected the mapped value.");
    }

    @Test
    public void testMappingByteObjectMinValue() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setByteObject(Byte.MIN_VALUE);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getByteObject(), Byte.valueOf(Byte.MIN_VALUE), "Expected the mapped value.");
    }

    @Test
    public void testMappingBooleanObjectFalse() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setBooleanObject(Boolean.FALSE);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getBooleanObject(), Boolean.FALSE, "Expected the mapped value.");
    }

    @Test
    public void testMappingBooleanObjectTrue() {
        FieldObjectEntity entity = new FieldObjectEntity();
        entity.setId(101L);
        entity.setBooleanObject(Boolean.TRUE);
        getEm().persist(entity);

        FieldObjectEntity foundEntity = getEm().find(FieldObjectEntity.class, 101L);
        Assert.assertNotNull(foundEntity, "Expected the entity to be found.");
        Assert.assertEquals(foundEntity.getBooleanObject(), Boolean.TRUE, "Expected the mapped value.");
    }

}
