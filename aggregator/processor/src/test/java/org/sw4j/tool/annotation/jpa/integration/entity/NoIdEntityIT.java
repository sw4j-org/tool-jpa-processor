/*
 * Copyright (C) 2016 Uwe Plonus
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.sw4j.tool.annotation.jpa.integration.entity;

import javax.xml.xpath.XPathExpressionException;
import org.sw4j.tool.annotation.jpa.integration.util.ITSuperclass;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.w3c.dom.Node;

/**
 * This class contains tests for the annotation {@code @Entity}.
 *
 * @author Uwe Plonus
 */
public class NoIdEntityIT extends ITSuperclass {

    /** Default constructor. */
    public NoIdEntityIT() {
    }

    @Test
    public void testEntityName() throws XPathExpressionException {
        Node entity = getNode("/model/entity[@name=\"NoIdEntity\"]");

        Assert.assertNotNull(entity, "Expected an entity with name \"NoIdEntity\" to exist.");
        Assert.assertEquals(entity.getNodeType(), Node.ELEMENT_NODE, "Expected an element.");
        Assert.assertEquals(getAttribute("name", entity).getNodeValue(),
                "NoIdEntity", "Expected the entity name to be \"NoIdEntity\"");
        Assert.assertEquals(getAttribute("className", entity).getNodeValue(),
                "org.sw4j.tool.annotation.jpa.entity.NoIdEntity",
                "Expected the class name to be \"org.sw4j.tool.annotation.jpa.entity.NoIdEntity\"");
    }

    @Test
    public void testEntityTable() throws XPathExpressionException {
        Node table = getNode("/model/entity[@name='NoIdEntity']/table[@name='NoIdEntity']");

        Assert.assertNotNull(table, "Expected a table with name \"NoIdEntity\" to exist.");
        Assert.assertEquals(table.getNodeType(), Node.ELEMENT_NODE, "Expected an element.");
        Assert.assertEquals(getAttribute("name", table).getNodeValue(),
                "NoIdEntity", "Expected the table name to be \"NoIdEntity\"");
        Assert.assertEquals(getAttribute("catalog", table).getNodeValue(), "",
                "Expected the catalog name to be \"\" (empty).");
        Assert.assertEquals(getAttribute("schema", table).getNodeValue(), "",
                "Expected the schema name to be \"\" (empty).");
    }

}
