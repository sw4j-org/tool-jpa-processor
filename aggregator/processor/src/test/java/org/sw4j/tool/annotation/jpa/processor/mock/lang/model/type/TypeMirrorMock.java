/*
 * Copyright (C) 2016 uwe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.sw4j.tool.annotation.jpa.processor.mock.lang.model.type;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVisitor;
import java.lang.annotation.Annotation;
import java.util.List;

/**
 *
 * @author Uwe Plonus
 */
public class TypeMirrorMock implements TypeMirror {

    private final TypeKind kind;

    public TypeMirrorMock(TypeKind kind) {
        this.kind = kind;
    }

    @Override
    public TypeKind getKind() {
        return this.kind;
    }

    @Override
    public <R, P> R accept(TypeVisitor<R, P> v, P p) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String toString() {
        switch (this.kind) {
            case BOOLEAN:
                return "boolean";
            case BYTE:
                return "byte";
            case CHAR:
                return "char";
            case DOUBLE:
                return "double";
            case FLOAT:
                return "float";
            case INT:
                return "int";
            case LONG:
                return "long";
            case SHORT:
                return "short";
            default:
                return "unknown";
        }
    }

    @Override
    public List<? extends AnnotationMirror> getAnnotationMirrors() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <A extends Annotation> A getAnnotation(Class<A> aClass) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public <A extends Annotation> A[] getAnnotationsByType(Class<A> aClass) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
